<?php
/**
 * themeplate enqueue scripts
 *
 * @package themeplate
 */


function themeplate_scripts() {
	$version = defined('WP_DEBUG')? time(): '1.0.0';
    wp_enqueue_style( 'themeplate-styles', get_stylesheet_directory_uri() . '/assets/css/theme.css', array(), $version);
    wp_enqueue_script('jquery'); 
    wp_enqueue_script( 'themeplate-scripts', get_template_directory_uri() . '/assets/js/theme.min.js', array('jquery'), $version, true );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}

add_action( 'wp_enqueue_scripts', 'themeplate_scripts' );
