<?php
/**
 * Declaring widgets
 *
 *
 * @package themeplate
 */
function themeplate_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'themeplate' ),
		'id'            => 'sidebar-1',
		'description'   => 'Sidebar widget area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 1', 'themeplate' ),
		'id'            => 'footer-1',
		'description'   => 'Widget area in the footer area 1',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 2', 'themeplate' ),
		'id'            => 'footer-2',
		'description'   => 'Widget area in the footer area 2',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer-3', 'themeplate' ),
		'id'            => 'footer-3',
		'description'   => 'Widget area in the footer area 3',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );

}

add_action( 'widgets_init', 'themeplate_widgets_init' );